﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace _2DGameToolKitTest
{
    using UnityAssertionExeption = UnityEngine.Assertions.AssertionException;

    public class ListenerTest
    {
        class DummyGameEvent : GameEvent
        {
            public DummyGameEvent() : base("Dummy", EProtocol.Instant)
            { }
        }

        class SecondDummyGameEvent : GameEvent
        {
            public SecondDummyGameEvent() : base("Dummy", EProtocol.Instant)
            { }
        }

        class ThirdDummyGameEvent : GameEvent
        {
            public ThirdDummyGameEvent() : base("Dummy", EProtocol.Instant)
            { }
        }

        class DummyListener
        {
            public DummyListener()
            {
                m_DummyEventReceived = false;
            }

            public void OnGameEvent(DummyGameEvent dummyEvent)
            {
                m_DummyEventReceived = true;
            }

            public void OnGameEvent(ThirdDummyGameEvent dummyEvent)
            {
                // This should not be allowed in an event callback
                this.RegisterAsListener("Dummy", typeof(DummyGameEvent));
            }

            public bool m_DummyEventReceived;
        }

        private GameEventManager m_EventManager = new GameEventManager();
        private readonly NullUpdater m_Updater = new NullUpdater();
        private DummyListener m_Listener = new DummyListener();

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            UnityEngine.Assertions.Assert.raiseExceptions = true;
            UpdaterProxy.Open(m_Updater);
            GameEventManagerProxy.Open(m_EventManager);
        }

        [SetUp]
        public void SetUp()
        {
            m_Listener = new DummyListener();
            m_EventManager.OnEngineStart();
        }

        [TearDown]
        public void TearDown()
        {
            m_EventManager.OnEngineStop();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            GameEventManagerProxy.Close(m_EventManager);
            UpdaterProxy.Close(m_Updater);
        }

        [Test]
        public void TestEventReception()
        {
            m_Listener.RegisterAsListener("Dummy", typeof(DummyGameEvent));

            new DummyGameEvent().Push();
            Assert.IsTrue(m_Listener.m_DummyEventReceived);
        }

        [Test]
        public void TestTag()
        {
            m_Listener.RegisterAsListener("Not Dummy", typeof(DummyGameEvent));

            new DummyGameEvent().Push();
            Assert.IsFalse(m_Listener.m_DummyEventReceived);
        }

        [Test]
        public void TestUnregister()
        {
            m_Listener.RegisterAsListener("Dummy", typeof(DummyGameEvent));
            Assert.Throws<UnityAssertionExeption>(delegate { m_Listener.UnregisterAsListener("Not Dummy"); });

            m_Listener.UnregisterAsListener("Dummy");
            new DummyGameEvent().Push();
            Assert.IsFalse(m_Listener.m_DummyEventReceived);
        }

        [Test]
        public void TestReflectionError()
        {
            m_Listener.RegisterAsListener("Dummy", typeof(SecondDummyGameEvent));
            Assert.Throws<UnityAssertionExeption>(delegate { new SecondDummyGameEvent().Push(); });
        }
    }
}
