﻿using NUnit.Framework;
using Actor;

namespace _2DGameToolKitTest
{
    using UnityAssertionExeption = UnityEngine.Assertions.AssertionException;

    public class StateAccessorTest
    {
        class DummyState : State<NullLocker>
        {
            private int m_Value = 0;

            public int GetValue()
            {
                return m_Value;
            }

            public void SetValue(int value)
            {
                m_Value = value;
            }
        }

        class DummyStateWithCheckLocker : State<CheckLocker>
        {
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            UnityEngine.Assertions.Assert.raiseExceptions = true;
        }

        [TearDown]
        public void TearDown()
        {
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
        }

        [Test]
        public void AccessToValidState()
        {
            DummyState state = new DummyState();
            using (var reader = new Reader<DummyState>(state))
            {
                Assert.IsTrue(reader.IsValid());
                Assert.IsTrue(reader.GetObject().GetValue() == 0);
            }
        }

        [Test]
        public void AccessToInvalidState()
        {
            using(var reader = new Reader<DummyState>(null))
            {
                Assert.IsFalse(reader.IsValid());
                Assert.Throws<UnityAssertionExeption>(delegate 
                    { reader.GetObject(); }
                );
            }
        }

        [Test]
        public void CheckPolicy()
        {
            DummyStateWithCheckLocker state = new DummyStateWithCheckLocker();
            using (var reader = new Reader<DummyStateWithCheckLocker>(state))
            {
                Assert.IsTrue(reader.IsValid());
                using (var otherReader = new Reader<DummyStateWithCheckLocker>(state))
                {
                    Assert.IsTrue(otherReader.IsValid());
                }
                Assert.Throws<UnityAssertionExeption>(delegate
                {
                    using (var writer = new Writer<DummyStateWithCheckLocker>(state))
                    {
                    }
                }
                );
            }
            using (var writer = new Writer<DummyStateWithCheckLocker>(state))
            {
                Assert.IsTrue(writer.IsValid());
                Assert.Throws<UnityAssertionExeption>(delegate
                {
                    using (var otherWriter = new Writer<DummyStateWithCheckLocker>(state))
                    {
                    }
                }
                ); 
                Assert.Throws<UnityAssertionExeption>(delegate
                {
                    using (var reader = new Reader<DummyStateWithCheckLocker>(state))
                    {
                    }
                }
                );
            }
        }
    }
}
