﻿using System;
using System.Collections.Generic;

namespace Actor
{
    public enum EActorStatus
    {
        Initializing,
        Ready,
        Invalid
    }

    public class Actor
    {
        private ActorGameID m_ActorGameID = (ActorGameID)GameID.InvalidGameID;
        private EActorStatus m_Status = EActorStatus.Invalid;
        private List<IState> m_States = new List<IState>();
        private List<IController> m_Controllers = new List<IController>();

        public void AddState<State>() where State : IState, new()
        {
            State state = new State();
            m_States.Add(state);
        }

        public void AddController<Controller>() where Controller : IController, new()
        {
            Controller controller = new Controller();
            controller.Init(this);
            m_Controllers.Add(new Controller());
        }

        internal State GetState<State>() where State : IState
        {
            Type type = typeof(State);
            return (State)m_States.Find(state => state.GetType() == type);
        }

        internal Controller GetController<Controller>() where Controller : IController
        {
            Type type = typeof(Controller);
            return (Controller)m_Controllers.Find(controller => controller.GetType() == type);
        }
    }
}
