﻿using System;
using UnityEngine.Assertions;

namespace Actor
{
    public enum EAccessType
    {
        Read,
        Write
    }

    public interface ILockedType
    {
        ILockPolicy GetLock();
    }

    public interface ILockPolicy
    {
        void Lock(EAccessType accessType);
        void Unlock(EAccessType accessType);
    }

    public class NullLocker : ILockPolicy
    {
        public void Lock(EAccessType accessType)
        {
        }

        public void Unlock(EAccessType accessType)
        {
        }
    }

    public class CheckLocker : ILockPolicy
    {
        private const Int32 m_SizeOfUInt16InBits = 8 * sizeof(UInt16);
        private static UInt32[] ms_AccessTypeOffset = { 1, 1 << m_SizeOfUInt16InBits };
        private UInt32 m_ReadWriteCount = 0;

        public void Lock(EAccessType accessType)
        {
            UInt32 futureCount = m_ReadWriteCount + ms_AccessTypeOffset[(int)accessType];
            Check(futureCount);
            m_ReadWriteCount = futureCount;
        }

        public void Unlock(EAccessType accessType)
        {
            UInt32 futureCount = m_ReadWriteCount - ms_AccessTypeOffset[(int)accessType];
            Check(futureCount);
            m_ReadWriteCount = futureCount;
        }

        private void Check(UInt32 futureCount)
        {
            UInt32 readCount = GetReadCount(futureCount);
            UInt32 writeCount = GetWriteCount(futureCount);
            Assert.IsTrue((writeCount == 1 && readCount == 0) || (writeCount == 0 && readCount >= 0));
        }

        private UInt32 GetReadCount(UInt32 count)
        {
            return (UInt16)count;
        }

        private UInt32 GetWriteCount(UInt32 count)
        {
            return count >> m_SizeOfUInt16InBits;
        }
    }
}
