﻿using UnityEngine.Assertions;

namespace Actor
{
    public class ActorReference
    {
        private Actor m_Actor;

        public ActorReference()
        {

        }

        public ActorReference(Actor actor)
        {
            m_Actor = actor;
        }

        public bool IsValid()
        {
            return m_Actor != null;
        }

        public Reader<State> GetStateReader<State>() where State : IState
        {
            return new Reader<State>(GetState<State>());
        }
        internal Writer<State> GetStateWriter<State>() where State : IState
        {
            return new Writer<State>(GetState<State>());
        }

        private State GetState<State>() where State : IState
        {
            Assert.IsTrue(IsValid(), "ActorReference is invalid, check IsValid before accessing it");
            return m_Actor.GetState<State>();
        }
    }
}
