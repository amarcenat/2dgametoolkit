﻿using UnityEngine.Assertions;
using System;

namespace Actor
{
    public class Storage<T> : IDisposable where T : ILockedType
    {
        private T m_Object;
        private EAccessType m_AccessType;

        public Storage(T obj, EAccessType accessType)
        {
            m_Object = obj;
            m_AccessType = accessType;
            if(IsValid())
            {
                m_Object.GetLock().Lock(m_AccessType);
            }
        }

        public ref T GetObject()
        {
            Assert.IsTrue(IsValid(), "Trying to access a null object");
            return ref m_Object;
        }

        public bool IsValid()
        {
            return m_Object != null;
        }

        #region IDisposable Support

        private bool m_HasBeenDisposed = false;

        private void Dispose(bool disposing)
        {
            if (!m_HasBeenDisposed)
            {
                if (IsValid())
                {
                    m_Object.GetLock().Unlock(m_AccessType);
                }
                if (disposing)
                {
                    m_Object = default(T);
                }
                m_HasBeenDisposed = true;
            }
        }

        ~Storage()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
