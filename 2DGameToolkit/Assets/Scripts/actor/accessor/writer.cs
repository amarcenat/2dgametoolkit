﻿namespace Actor
{
    public sealed class Writer<State> : Storage<State> where State : IState
    {
        public Writer(State state) : base(state, EAccessType.Write)
        {}
    }
}
