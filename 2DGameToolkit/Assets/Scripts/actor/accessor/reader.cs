﻿namespace Actor
{
    public sealed class Reader<State> : Storage<State> where State : IState
    {
        public Reader(State state) : base(state, EAccessType.Read)
        {}
    }
}
