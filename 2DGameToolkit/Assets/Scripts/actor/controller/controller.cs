﻿using System;

namespace Actor
{
    public interface IController
    {
        void Init(Actor actor);
        void Link();
        void Unlink();
        void Shutdown(Actor actor);
    }

    public class Controller : IController
    {
        private ActorReference m_Actor = new ActorReference();

        public virtual void Init(Actor actor)
        {
            m_Actor = new ActorReference(actor);
        }

        public void Link()
        {
            throw new NotImplementedException();
        }

        public void Unlink()
        {
            throw new NotImplementedException();
        }

        public void Shutdown(Actor actor)
        {
            throw new NotImplementedException();
        }

        protected Reader<State> GetStateReader<State>() where State : IState
        {
            return m_Actor.GetStateReader<State>();
        }

        protected Writer<State> GetStateWriter<State>() where State : IState
        {
            return m_Actor.GetStateWriter<State>();
        }
    }
}
