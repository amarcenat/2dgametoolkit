﻿using System;

namespace Actor
{
    using Raw = UInt64;
    using MainHash = UInt64;
    using SystemType = ESystemType;
    using ObjectType = UInt16;

    public class GameID
    {
        private const int K_MAIN_BITS = 48;
        private const int K_SYSTEMTYPE_BITS = 8;
        private const int K_OBJECTTYPE_BITS = 8;

        private const int K_MAIN_OFFSET = 0;
        private const int K_SYSTEMTYPE_OFFSET = K_MAIN_BITS;
        private const int K_OBJECTTYPE_OFFSET = K_SYSTEMTYPE_OFFSET + K_SYSTEMTYPE_BITS;

        private const Raw K_MAIN_MASK = (1 << K_MAIN_BITS) - 1;
        private const Raw K_SYSTEMTYPE_MASK = ((1 << K_SYSTEMTYPE_BITS) - 1) << K_SYSTEMTYPE_OFFSET;
        private const Raw K_OBJECTTYPE_MASK = ((Raw)(1 << K_OBJECTTYPE_BITS) - 1) << (K_OBJECTTYPE_OFFSET);

        private Raw m_Raw = 0;

        public MainHash GetMainHash()
        {
            return m_Raw & K_MAIN_MASK;
        }

        protected void SetMainHash(MainHash mainHash)
        {
            if(this == InvalidGameID)
            {
                return;
            }
            Raw mainHashRaw = ((Raw)mainHash) << K_MAIN_OFFSET;
            m_Raw &= ~K_MAIN_MASK;
            m_Raw &= mainHashRaw;
        }

        public SystemType GetSystemType()
        {
            return (SystemType)(m_Raw & K_SYSTEMTYPE_MASK);
        }

        protected void SetSystemType(SystemType systemType)
        {
            if (this == InvalidGameID)
            {
                return;
            }
            Raw systemTypeRaw = ((Raw)systemType) << K_SYSTEMTYPE_OFFSET;
            m_Raw &= ~K_SYSTEMTYPE_MASK;
            m_Raw &= systemTypeRaw;
        }

        public ObjectType GetObjectType()
        {
            return (ObjectType)(m_Raw & K_OBJECTTYPE_MASK);
        }

        protected void SetObjectType(ObjectType objectType)
        {
            if (this == InvalidGameID)
            {
                return;
            }
            Raw objectTypeRaw = ((Raw)objectType) << K_OBJECTTYPE_OFFSET;
            m_Raw &= ~K_OBJECTTYPE_MASK;
            m_Raw &= objectTypeRaw;
        }

        public Raw GetRaw()
        {
            return m_Raw;
        }

        public GameID(MainHash mainHash, SystemType systemType, ObjectType objectType)
        {
            SetMainHash(mainHash);
            SetSystemType(systemType);
            SetObjectType(objectType);
        }

        public static GameID InvalidGameID = new GameID(0, SystemType.Invalid, 0);
    }

}
