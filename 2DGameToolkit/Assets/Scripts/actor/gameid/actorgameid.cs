﻿using System;

namespace Actor
{
    public class ActorGameID : GameID
    {
        public ActorGameID(UInt64 mainHash, ESystemType systemType, UInt16 actorType) 
            : base(mainHash, systemType, actorType)
        {

        }

        public UInt16 GetActorType()
        {
            return (UInt16)GetObjectType();
        }
    }
}
