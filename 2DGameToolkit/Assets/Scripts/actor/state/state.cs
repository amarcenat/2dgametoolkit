﻿namespace Actor
{
    public interface IState : ILockedType
    {
    }

    public class State<LockPolicy> : IState where LockPolicy : ILockPolicy, new()
    {
        internal static int ms_CurrentStamp = 0;

        private LockPolicy m_Lock = new LockPolicy();
        private int m_DirtyStamp = 0;

        public ILockPolicy GetLock()
        {
            return m_Lock;
        }

        public bool IsDirty()
        {
            return m_DirtyStamp >= ms_CurrentStamp;
        }

        protected void StampDirty()
        {
            m_DirtyStamp = ms_CurrentStamp;
        }
    }
}
