﻿
using UnityEngine;
using UnityEngine.Assertions;

using AnyObject = System.Object;

public interface IAsyncRequest
{
    void OnObjectLoaded();
}

public class AsyncRequest<T> : IAsyncRequest
{
    private AnyObject m_Caller;
    private Coroutine m_LoadRoutine;
    private T m_LoadedObject;
    private bool m_IsDone = false;

    public AsyncRequest(AnyObject caller)
    {
        m_Caller = caller;
        m_LoadRoutine = null;
        m_LoadedObject = default(T);
    }

    public void SetRoutine(Coroutine loadRoutine)
    {
        m_LoadRoutine = loadRoutine;
    }

    public void SetLoadedObject(T loadedObject)
    {
        m_LoadedObject = loadedObject;
    }

    public T GetObject()
    {
        return m_LoadedObject;
    }

    public bool IsDone()
    {
        return m_IsDone;
    }

    public void Dispose()
    {
        if (m_LoadRoutine != null)
        {
            this.StopGlobalCoroutine(m_LoadRoutine);
        }
        m_Caller = null;
        m_LoadRoutine = null;
        m_LoadedObject = default(T);
    }

    public void OnObjectLoaded()
    {
        Assert.IsTrue(m_LoadedObject != null, "OnObjectLoaded called but object is null");
        if (m_Caller != null)
        {
            ReflectionHelper.CallMethod("OnObjectLoaded", m_Caller, m_LoadedObject);
        }
        m_IsDone = true;
    }
}