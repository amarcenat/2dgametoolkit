﻿
public class Listener
{
    private System.Object m_ObjectToNotify;
    private System.Type[] m_GameEventTypes;

    public Listener(System.Object objectToNotify, params System.Type[] GameEventTypes)
    {
        m_ObjectToNotify = objectToNotify;
        m_GameEventTypes = GameEventTypes;
    }

    public System.Object GetObjectToNotify()
    {
        return m_ObjectToNotify;
    }

    public bool IsGameEventHandled(GameEvent e)
    {
        System.Type GameEventType = e.GetType();
        foreach (System.Type type in m_GameEventTypes)
        {
            if (type == GameEventType)
            {
                return true;
            }
        }
        return false;
    }
}