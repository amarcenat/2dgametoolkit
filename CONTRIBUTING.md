# Contributing 

## Intended design

The goal is to provide a user friendly, easy to debug framework, with a bunch of basic gameplay element that every game need.
To that extent a declarative syntax is enforced using reflection. This is not great for performance but it does not matter for 2D games made for a game jam. Debugging is made easier with the help of assertion, unittest, and the event base structure that allow easy decoupling.
This project should remain game agnostic as much as possible.

## Naming

| Item          | Prefix  |   Case     |
|---------------|---------|------------|
| class         |         | PascalCase |
| method        |         | PascalCase |
| Interface     |   I     | PascalCase |
| Enum          |   E     | PascalCase |
| static member |   ms_   | PascalCase |
| other member  |   m_    | PascalCase |

## Brackets

Always place on a newline

## if statement

Always use brackets even if their is only one line:

if(condition)
{
    a = 1;
}

## Comments

Always use // for one line and /** **/ for multiline.

## UnitTest

Unit testing is done with the **MSTest** framework.
You will find unit test in the **2DGameToolKit/** folder.
Only Unity agnostic functionalities is tested (i.e. scripts in the **Engine/** folder).
Please always make torough test when you add new features, as it provides both stability and a good example of how to use your code.

## Documentation

Documentation is done with **Doxygen**.
Please always document your code using the /** **/ comment style.
Do not document self explanatory method such as getter / setter.
