# 2DGameToolkit

A 2D game toolkit for game jam purposes.

## Summary

* Event system with a user friendly syntax using reflection to avoid boiler plate code
* Update system which allow to split the classic Unity Update into as many pass as you want, to ensure the order of execution. This also avoid boiler plate code (using reflection again).
* An HSM (Hybrid State Machine) system, to control the gameflow, the AI...
* An input system, that allow user reconfiguration
* A simple sound manager
* A simple dialogue system
* A simple level generator that allows to script events like spawning, changing music, trigger dialogue
* A 2D game camera with zoom, shaking, focusing, and lane features.
* A bunch of common gameplay elements, such as health, moving object, weapon system, bullets...
* Several menus : main menu, input configuration, pause, end level, game over.
* A bunch of syntaxic sugar using extension methods

## How to use it

Check the doxygen documentation.
The unittest (in **2DGameToolKit/** folder) also provides examples of how to use the code.
Open the scene MainScene in Unity for a demo of all the features.

## Folder structure

The scripts are split into three pretty self explanatory folders:
* Engine : here are all the game agnostic scripts.
* Gameplay : here is where to put your all the game specific scripts. A few common scripts and a basic gameflow are already there, they give you an idea of ow to use the toolkit.
* UI : all the UI related scripts, some basic menu are already there

## Intended use

This toolkit is made for event base design. You should rely on event to maintain nice decoupled architecture. The UI code for instance has to be removable without changing any gameplay related code.
AI is supposed to be driven with the HSM system.